import apps.QuizIST;

import java.util.HashMap;
import java.util.Map;

public class MainApps {
    public static void main(String[] args) throws Exception {
        Map<Double, Integer> voucher = new HashMap<Double, Integer>();
        QuizIST application = new QuizIST();
        voucher.put(10000D, 100);
        voucher.put(25000D, 200);
        voucher.put(50000D, 400);
        voucher.put(100000D, 800);

        System.out.println("===============================================");
        System.out.println("-----------Java Quiz Loyalti Program-----------");
        System.out.println("===============================================");
        System.out.println("1. Voucher dengan poin terbesar: ");
        application.highestPoint(voucher).forEach((key, value) -> {
            System.out.println(key + " = " + value + "p");
        });
        System.out.println("\n2. Sisa poin redeem dgn point terbesar 1000p: ");
        System.out.println(application.redeemWithTheHighestPoint(1000, voucher) + "point");
        System.out.println("\n3. Redem all point dgn prioritas poin terbesar");
        application.redeemWithHighestPointPriority(1150, voucher).forEach((key, value) -> {
            System.out.print(key + ":" );
            System.out.println(value);
        });
        System.out.println("===============================================");
    }

}
