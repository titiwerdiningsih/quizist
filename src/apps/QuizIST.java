package apps;

import java.util.*;
import java.util.stream.Collectors;

public class QuizIST {
    public Map<String, Integer> highestPoint(Map<Double, Integer> voucherMap) {
        Map<String, Integer> result= new HashMap<>();
        int maxValue = (Collections.max(voucherMap.values()));
        String key = null;
        for(Map.Entry<Double, Integer> entry : voucherMap.entrySet()) {
            if(entry.getValue().equals(maxValue)) {
                key = "Rp. "+ entry.getKey().intValue();
                result.put(key, maxValue);
            }
        }
        return result;
    }

    public Map<String, Integer> redeemWithHighestPointPriority(int currentPoint, Map<Double, Integer> voucherMap) {
        Map<String, Integer> result = new LinkedHashMap<>();
        List<Integer> highestPoint = voucherMap.values().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        List<Double> highestKey = voucherMap.keySet().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        for(int i=0; i<highestKey.size(); i++) {
            if(highestKey.get(i) <= currentPoint) {
                currentPoint = currentPoint - highestPoint.get(i);
                if(Boolean.FALSE.equals(result.containsKey("Rp. " + highestKey.get(i).intValue()+" "))) {
                    result.put("Rp. " + highestKey.get(i).intValue()+ " ", 1);
                }else{
                    result.replace("Rp. " + highestKey.get(i).intValue() + " ", result.get("Rp. " + highestKey.get(i).intValue() + " ")+1);
                }
            }else{
                i++;
            }
        }

        result.put("poin sekarang ", currentPoint);
        return result;
    }

    public int redeemWithTheHighestPoint(int currentPoint, Map<Double, Integer> redeemPoints) {
        int highestPoint = (Collections.max(redeemPoints.values()));
        int result = currentPoint - highestPoint;
        return result;
    }

}
